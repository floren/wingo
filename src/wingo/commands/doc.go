/*
package commands defines the Gribble command environment for Wingo.

For more about Gribble, see 
http://godoc.burntsushi.net/pkg/gribble/

For about using Gribble with Wingo, see
https://wingo/blob/master/HOWTO-COMMANDS
*/
package commands
