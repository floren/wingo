#!/bin/bash

mkdir -p ~/.local/share/wingo/
cp -r src/wingo/data/* ~/.local/share/wingo

mkdir -p ~/bin
cp bin/wingo ~/bin/
