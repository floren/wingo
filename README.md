Wingo
---------

Re-organized wingo (github.com/BurntSushi/wingo) that is self-sufficient,
containing all of its dependencies rather than fetching them with "go
get". This should help prevent breakages if someone decides to delete
their repo or whatever.

1. Download repo
2. ./all.bash
3. ./install.bash to install wingo for the current user
4. The wingo binary should be in ~/bin. Set up your .xsession or whatever
   and have fun!
